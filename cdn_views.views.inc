<?php

/**
 * Implements hook_views_post_render().
 */
function cdn_views_views_post_render(&$view, &$output, &$cache) {
  module_load_include('inc', 'cdn', 'cdn.fallback');

  cdn_html_alter_image_urls($output);
}

