@author Jordan Starcher
@project http://drupal.org/project/cdn_views

This module is a lightweight add-on to the CDN module which will rewrite the URLs of any images generated within a View.

Installation:
Assuming you have the CDN module installed and configured properly, simple install the CDN Views module as any other module and you should see your Views output rewriten URLs. There is not an admin UI to this module or any sort of configuration.
